/* eslint-disable no-undef */
import Twitter, { AccessTokenOptions } from 'twitter';

export default class TwitterService {
    private config: AccessTokenOptions;
    private client: Twitter;

    constructor(config?: AccessTokenOptions) {
        if (config) {
            this.config = config;
        } else {
            this.config = {
                consumer_key: process.env.TWITTER_CONSUMER_KEY ?? '',
                consumer_secret: process.env.TWITTER_CONSUMER_SECRET ?? '',
                access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY ?? '',
                access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET ?? ''
            };
        }

        this.client = new Twitter(this.config);
    }

    public postTweet(tweetText: string): Promise<Twitter.ResponseData> {
        return this.client.post('statuses/update', { status: tweetText });
    }

}