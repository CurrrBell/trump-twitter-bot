
export function fixNonWords(tweetText:String) {
    let tweetWords = tweetText.split(' ');
    tweetWords = tweetWords.map((word) => {
        return word == '&amp;' ? '&' : word;
    });
    tweetWords = tweetWords.filter((word) => !word.includes('http'));
    return tweetWords.join(' ');
}

