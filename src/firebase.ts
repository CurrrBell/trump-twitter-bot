import * as firebase from 'firebase-functions';
import cors from 'cors';

import TwitterService from './Services/TwitterService';
import { getMostRecentTrumpTweets, getMostRecentGeneratedTweets, saveGeneratedTweet } from './Services/FirestoreService';
import { makeTweets } from './Factories/TrumpTweetFactory';
import { generateText } from './Services/TweetGeneratorService';
import TrumpLexicon from './Models/TrumpLexicon';
import TrumpTweet from './Models/TrumpTweet';
import { filterTweets } from './Services/TweetFilterService';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const generateNewTweet = firebase.pubsub.schedule('0 */7 * * *').onRun(async (context) => {

    try {
        const twitterConfig = firebase.config().twitter;
        const twitter = new TwitterService(twitterConfig);

        const mostRecentTweets = await getMostRecentTrumpTweets();

        const tweets = makeTweets(mostRecentTweets);
        const filteredTweets = filterTweets(tweets);
        const tweetSubset = getRandomTweets(filteredTweets);
        const lexicon = new TrumpLexicon(tweetSubset.map((tweet) => tweet.text));
        const tweetBody = generateText(lexicon);

        twitter.postTweet(tweetBody);

        saveGeneratedTweet(tweetBody, tweetSubset);

        return null;
    }
    catch (error) {
        console.error('generateNewTweet failed: ', error);
        return null;
    }

});

export const getGeneratedTweets = firebase.https.onRequest(async(request, response) => {
    response.set('Access-Control-Allow-Origin', '*');   // TODO: change this once chrisbell.io goes live

    try {

        const tweets = await getMostRecentGeneratedTweets(request.params['numberToRetrieve']);

        response.send(tweets);

    } catch (error) {
        console.error('getMostRecentGeneratedTweets failed: ', error);
        response.status(500).send('An error occurred while fetching tweets');
    }

});

function getRandomTweets(tweets: TrumpTweet[], numberToGet: number = 5): TrumpTweet[] {
    let tweetsToReturn: TrumpTweet[] = [];

    while (tweetsToReturn.length < numberToGet) {
        const randomIndex = Math.floor(Math.random() * tweets.length);

        const randomTweet = tweets[randomIndex];

        if (!tweetsToReturn.includes(randomTweet)) {
            tweetsToReturn.push(randomTweet);
        }
    }

    return tweetsToReturn;
}