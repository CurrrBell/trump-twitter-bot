import { expect } from 'chai';
import { describe, it, beforeEach, before } from 'mocha';
import TrumpLexicon from '../src/Models/TrumpLexicon';
import { generateText, MAX_TWEET_LENGTH } from '../src/Services/TweetGeneratorService';

describe('TweetGeneratorService', () => {

    let lexicon: TrumpLexicon;
    let generatedTweets: string[] = [];
    const numberOfTweetsToGenerate = 1000;

    const testSentences = [
        'Improve your goldfish\'s physical fitness by getting him a bicycle.',
        'Just go ahead and press that button.',
        'As you consider all the possible ways to improve yourself and the world, you notice John Travolta seems fairly unhappy.',
        'The skeleton had skeletons of his own in the closet.',
        'She always speaks to him in a loud voice.',
        'He had a wall full of masks so she could wear a different face every day.',
        'She had convinced her kids that any mushroom found on the ground would kill them if they touched it.',
        'There\'s a message for you if you look up.',
        'You have every right to be angry, but that doesn\'t give you the right to be mean.',
        'At that moment he wasn\'t listening to music, he was living an experience.',
        'The beauty of the sunset was obscured by the industrial cranes.',
        'I caught my squirrel rustling through my gym bag.',
        'A dead duck doesn\'t fly backward.',
        'He went back to the video to see what had been recorded and was shocked at what he saw.',
        'It took him a while to realize that everything he decided not to change, he was actually choosing.',
        'Various sea birds are elegant, but nothing is as elegant as a gliding pelican.',
        'Her scream silenced the rowdy teenagers.',
        'My Mum tries to be cool by saying that she likes all the same things that I do.',
        'There were white out conditions in the town; subsequently, the roads were impassable.',
        'Today I dressed my unicorn in preparation for the race.',
        'Mary plays the piano.',
        'It was the first time he had ever seen someone cook dinner on an elephant.',
        'I’m a living furnace.',
        'The elephant didn\'t want to talk about the person in the room.',
        'Honestly, I didn\'t care much for the first season, so I didn\'t bother with the second.',
        'The bullet pierced the window shattering it before missing Danny\'s head by mere millimeters.',
        'She wanted to be rescued, but only if it was Tuesday and raining.',
        'You\'ll see the rainbow bridge after it rains cats and dogs.',
        'The sign said there was road work ahead so he decided to speed up.',
        'All she wanted was the answer, but she had no idea how much she would hate it.',
        'Nothing is as cautiously cuddly as a pet porcupine.',
        'She had some amazing news to share but nobody to share it with.',
        'Last Friday I saw a spotted striped blue worm shake hands with a legless lizard.',
        'Although it wasn\'t a pot of gold, Nancy was still enthralled at what she found at the end of the rainbow.',
        'I was very proud of my nickname throughout high school but today- I couldn’t be any different to what my nickname was.',
        'Pink horses galloped across the sea.',
        'I come from a tribe of head-hunters, so I will never need a shrink.',
        'The blue parrot drove by the hitchhiking mongoose.',
        'Italy is my favorite country; in fact, I plan to spend two weeks there next year.',
        'The waitress was not amused when he ordered green eggs and ham.',
        'The memory we used to share is no longer coherent.',
        'The old apple revels in its authority.',
        'People who insist on picking their teeth with their elbows are so annoying!',
        'She works two jobs to make ends meet; at least, that was her reason for not having time to join us.',
        'If you don\'t like toenails, you probably shouldn\'t look at your feet.',
        'Henry couldn\'t decide if he was an auto mechanic or a priest.',
        'She traveled because it cost the same as therapy and was a lot more enjoyable.',
        'Purple is the best city in the forest.',
        'He knew it was going to be a bad day when he saw mountain lions roaming the streets.',
        'I liked their first two albums but changed my mind after that charity gig.'
    ];

    before(() => {
        lexicon = new TrumpLexicon(testSentences);
    });

    beforeEach(() => {
        generatedTweets = [];

        for (let i = 0; i < numberOfTweetsToGenerate; i++) {
            generatedTweets.push(generateText(lexicon));
        }
    });

    it('should never generate a tweet longer greater than the max tweet length', () => {
        expect(generatedTweets.every((tweet) => tweet.length <= MAX_TWEET_LENGTH)).to.be.true;
    });

    it('should never generate a tweet with leading or trailing whitespace', () => {
        expect(generatedTweets.every((tweet) => tweet.length == tweet.trim().length)).to.be.true;
    });

});